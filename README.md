# dodo-server

#### requirements

>Node: **16.13.1**
>
>NATS server

## SETUP

```
yarn add dodo-server@https://gitlab.com/odo.konrad/dodo-server
```

### create file /config.js -> its a config of all microservice in your project.
```javascript
module.exports = [
  {
    name: 'test-service',
    script: './service/test-service',
  }, 
  {
    name: 'gateway',
    script: './service/gateway',
  },
];
```

### next create folder structure
```
.
* service
    * gateway
    * test-service
       * action
       * repo -> opcional
       * validator
       * routing
```


### gateway setup -> /service/gateway/index.js
```javascript
const setupGateway = require('dodo-server/setupGateway');
const config = require('../../config');
const { checkSession } = require('../../setup/auth');

setupGateway({
  name: 'gateway', // this name must be the same like in /config.js
  port: 8884,
  microservice_config: config, // this is config of all services in project
  checkSession, // check session its explain under
});

// TODO add option to rest configuration
```

## Service configuration

### action -> /service/test-service/action/test.action.js
```javascript
const test = async ({ body, session }) => { // action trigerred by endpoint
    console.log('hello world', body)
    return { message: 'hello world'} // response of endpoint
};

module.exports = {
  test,
};
```

### validator -> /service/test-service/validator/test.validator.js
```javascript
const { ValidatorHelper } = require('dodo-server/validatorHelper');

const testValidator = { // this is a AJV validator -> check documentation
    properties: {
        name: ValidatorHelper.name,
        age: ValidatorHelper.string,
    },
    required: ['email', 'password'],
};

module.exports = { testValidator };
```

### routing -> /service/test-service/routing/test.routing.js
```javascript
const { METHOD_TYPE } = require('dodo-server');
const testProvider = require('../endpoint/test.action');
const testValidator = require('../validator/test.validator');

module.exports = [
    {
        method: METHOD_TYPE.POST, // chose one of the method (recomended POST & WS)
        action: authProvider.test, // chose action
        validator: authValidator.test, //chose validator for body
        path: 'login', // it's subpath for endpoint, if service had name 'test-service' then path is 'test-service/login'
        login: false, // if set true -> then user nead session -> session is install in gateway
        roles: [1 , 2], // check roles from session -> TODO,
        private: true, // default false -> if true endpoint is only for microservice connection
    },
    ... other endpoint
];

```

### last step is the setup MicroService -> /service/test-service/index.js
```javascript
const setupMicroService = require('dodo-server/setupMicroservice');
const test = require('./routing/test.route');

const service_name = 'test-service'; // the name must be the same like is in config

const endpoint_config = [
    ...test,
    // here add another routing file
];

setupMicroService({ service_name, endpoint_config });
```

## WEBSOCKET EXAMPLE

```javascript
module.exports = [
  {
    method: METHOD_TYPE.POST,
    action: provider.getHistory,
    validator: validator.getHistory,
    path: 'getHistory',
    login: true,
  },
  {
    method: METHOD_TYPE.WS, // SET WS METHOD
    path: 'conversation', // ITS path for WS
    room_name: true, // if true -> ws nead room in body, if false -> its global ws channel
    subscribe: {  // action on subscibe method -> can be empty
      action: provider.onSubscribe,
      validator: {},
    },
    message: {
      action: provider.onMessage, // action on all message
    },
    unsubscribe: { // action on unsubscribe method -> can be empty
        
    },  
    login: true,
  },
];
```

```javascript
const onMessage = async (req) => {
  const { message } = req.body;
  const { user_id, room } = req.session; // room is in session
  const { id: room_id } = await roomRepo.get({ room });
  const { id } = await messageRepo.create({ message, user_id, room_id });
  return messageRepo.get({ id });
};
```

## HOW CALL TO ANOTHER MICROSERVICE 

> use function callToService, remember, you can call only to `private` action

```javascript
const getUserFromRoom = async (req, { callToService }) => {
  const { room } = req?.body;
  const { id: room_id } = await roomRepo.get({ room });
  const users = await userRoomRepo.list({ filter: { room_id } }, { all_item: true });
  const user_ids = users.map(({ user_id }) => user_id);
  const { success, result } = await callToService('account', 'getUserByIds', { user_ids });
  if (success) {
    return result;
  }
  throw { message: 'ERROR - PO PROSTU' };
};
```

## Handling Error

> if you want return a error use `throw` with specify object

```javascript
throw { message: 'error message' };
```

| Key        | default        |
|------------|----------------|
| message    | Internal Error |
| status     | 500            |
| error_code | 0              |