const {StringCodec} = require("nats");

const sc = StringCodec()

const METHOD_TYPE = {
    POST: 'post',
    GET: 'get',
    DELETE: 'delete',
    PATCH: 'patch',
    WS: 'ws'
}

const natsEncodeMessage = (message) => {
    return sc.encode(JSON.stringify(message || {}))
}

const natsDecodeMessage = (message) => {
    return JSON.parse(sc.decode(message.data) || {})
}

module.exports = {
    METHOD_TYPE,
    natsEncodeMessage,
    natsDecodeMessage,
}
