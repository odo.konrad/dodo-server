const nanoExpress = require('nanoexpress');
const bodyParser = require('@nanoexpress/middleware-body-parser/cjs');
const cors = require('cors');
const validation = require("./validator");
const { natsDecodeMessage, natsEncodeMessage, METHOD_TYPE } = require('./index');
const { StringCodec, connect } = require('nats');
const staticServe = require('@nanoexpress/middleware-static-serve/cjs');

const default_cors_option = {
  origin: '*',
  allowHeaders: ['content-type', 'x-auth-token'],
  methods: ['GET', 'POST'],
  // preflightContinue: true,
};
const sc = StringCodec();

const getMicroserviceConfig = async (server, mc_config) => {
  try {
    const mc_endpoint_config = mc_config.map(({ name }) => server.request(`${name}-config`, sc.encode('empty'), { timeout: 1000 }));
    const messages_list = await Promise.all(mc_endpoint_config);

    return messages_list.flatMap((msg, index) => {
      const service = mc_config[index].name;
      const endpoints = natsDecodeMessage(msg);
      const without_private = endpoints.filter(({ private }) => !private)

      return without_private.map((endpoint) => ({
        service,
        route: `/${service}/${endpoint.path}`,
        method: endpoint.method || METHOD_TYPE.POST,
        ...endpoint,
      }));
    });
  } catch (e) {
    console.error('ERROR - RELOAD SERVER CONFIG');
    return getMicroserviceConfig(server, mc_config);
  }
};

const setupGateway = async ({
  name = 'ws-gateway',
  microservice_config,
  port,
  checkSession = () => {},
  cors_option = {},
  static,
}) => {
  const app = nanoExpress();
  app.use(bodyParser());
  app.use(cors({ ...default_cors_option, ...cors_option }));
  if (static) {
    app.use(staticServe(static));
  }
  const server = await connect({ port: 4222 });
  const service_list = microservice_config.filter((service) => service.name !== name);

  const config = await getMicroserviceConfig(server, service_list);

  const decoder = new TextDecoder("utf-8");
  const encoder = new TextEncoder("utf-8");
  app.get('/config', (req, res) => {
    res.send({ config });
  });

  config?.forEach((endpoint) => {
    if(endpoint.method !== METHOD_TYPE.WS){
      app[endpoint.method](endpoint.route, async (req, res) => {
        try {
          const { host, 'user-agent': user_agent, 'x-auth-token': token } = req.headers;
          const { params = {}, body = {} } = req;
          const user = await checkSession(token);

          if (endpoint.login && !user) {
            throw { code: 401, message: 'unauthorized' };
          }

          const payload = {
            data: { params, body, host, session: { user_agent, host, ...user }},
            path: endpoint.path,
            token,
          };
          // TODO role validator
          if (endpoint?.validator) {
            const valid = validation(endpoint.validator, body);
            if (valid.is_err) throw { message: 'body is not valid', errors: valid.errors };
          }

          const messages = await server.request(endpoint.service, natsEncodeMessage(payload), { timeout: 10000 });
          const parse_massage = natsDecodeMessage(messages);
          if (parse_massage.success) {
            res.send(parse_massage);
          } else {
            throw parse_massage.result;
          }
        } catch (e) {
          console.error(e);
          e.message = e.message || 'INTERNAL ERROR',
          e.error_code = e.error_code || 0;

          res.status(e.status || 500).send({
            success: false,
            ...e,
          });
        }
      });
    } else {

      app[endpoint.method](endpoint.route, async (req, res) => {

        res.on('connection', async (ws) => {
          if (endpoint?.subscribe) {
            ws.send(JSON.stringify({ success: true } || {}))
          }

          ws.on('message', async (msg) => {
            // eslint-disable-next-line security-node/detect-crlf
            console.log('Message received', msg);

            if (msg === 'PING') {
                ws.send('PONG');
                return
              }

              // TODO validator
              const { token, type, room, ...body } = JSON.parse(msg)
              const user = await checkSession(token);

              if (endpoint.login && !user) {
                ws.send(JSON.stringify({ code: 401, message: 'unauthorized' }));
                ws.close()
              }

              if (endpoint?.validator) {
                const valid = validation(endpoint.validator, body);
                ws.send(JSON.stringify({ message: 'body is not valid', errors: valid.errors }));
              }

              const topic = endpoint.route + ';' + room || 'all'

              const payload = {
                data: { body, session: { ...user, room } },
                path: endpoint.path,
                type,
              };

              if (type === 'subscribe') {
                ws.subscribe(topic);
                ws.send(JSON.stringify({ type, success: true }));
              }

              if (type === 'message') {
                const messages = await server.request(endpoint.service, natsEncodeMessage(payload), { timeout: 10000 });
                const parse_massage = natsDecodeMessage(messages);

                const res = JSON.stringify({type, ...parse_massage})
                ws.publish(topic, res);
                ws.send(res);
              }
          });
        });
      });
    }
  });

  app.listen(port);
};

module.exports = setupGateway;
