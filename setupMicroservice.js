const {METHOD_TYPE} = require("./index");
const { natsEncodeMessage } = require('./index');
const { natsDecodeMessage } = require('./index');
const { connect, StringCodec } = require('nats');

const sc = StringCodec();

const setupMicroService = async (
  {
    service_name = 'default-name',
    endpoint_config = [],
    nats_port = 4222,
  },
) => {
  const server = await connect({ port: nats_port });

  const callToService = async (service, endpoint, body) => {
    const payload = {
      data: { body },
      path: endpoint,
    };
    console.log({ payload, body })
    const messages = await server.request(service, natsEncodeMessage(payload), { timeout: 10000 });
    return natsDecodeMessage(messages);
  }

  server.subscribe(service_name, {
    callback: async (err, msg) => {
      try {
        console.log(msg)
        const { path, data, session, type } = natsDecodeMessage(msg);
        console.log({ data })
        const endpoint = endpoint_config.find((endpoint) => endpoint.path === path);
        if (!endpoint) throw { message: 'route not exist' };
        const additional = { callToService }

        if (endpoint.method === METHOD_TYPE.WS){
          const result = await endpoint[type].action(data, additional);
          msg.respond(natsEncodeMessage({ success: true, result }));
        } else {
          const result = await endpoint.action(data, additional);
          msg.respond(natsEncodeMessage({ success: true, result }));
        }

      } catch (error) {
        console.error({ error });
        msg.respond(natsEncodeMessage({ success: false, result: error }));
      }
    },
  });

  server.subscribe(`${service_name}-config`, {
    callback: (err, msg) => {
      const config_to_send = endpoint_config.map(({ action, ...payload }) => payload);
      const payload = JSON.stringify(config_to_send);
      msg.respond(sc.encode(payload));
    },
  });

  console.log(`SERVER - ${service_name} IS RUNNING`);
};

module.exports = setupMicroService;
