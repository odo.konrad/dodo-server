const nanoExpress = require('nanoexpress');
const { resolve } = require('path');
const staticServe = require('@nanoexpress/middleware-static-serve/cjs');



const setupStaticServer = async (path_file, option) => {
    const app = nanoExpress();
    app.use(

        staticServe(resolve(path_file), {
            mode: 'live',
            compressed: false,
            addPrettyUrl: false,
            ...option
        })
    );

    app.get('/*', (req, res) => {
        res.send(true)
    })
    app.listen(4002);
}

module.exports = setupStaticServer;
