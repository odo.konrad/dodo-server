const Ajv = require("ajv");

const ajv = new Ajv({
    removeAdditional: 'all',
    validateSchema: false,
    additionalProperties: false,
    jsonPointers: true,
    messages: false,
    $data: true,
    allErrors: true,
});

const validation = (schema, data) => {
    const is_error = !ajv.validate(schema, data);
    const { errors } = ajv;

    return {
        is_err: is_error,
        errors,
    };
};

module.exports = validation;
