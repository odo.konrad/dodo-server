const ValidatorHelper = {
    uid: {
        type: 'string',
        minLength: 17,
        maxLength: 17,
        pattern: '^765\\d+$',
    },
    number: {
        type: 'number',
    },
    bit: {
        type: 'integer',
        enum: [0, 1],
    },
    numberOrNull: {
        type: ['number', 'null'],
    },
    string: {
        type: 'string',
    },
    stringOrNull: {
        type: ['string', 'null'],
    },
    stringNotEmpty: {
        type: 'string',
        minLength: 1,
    },
    stringOrArrayOfStrings: {
        type: ['string', 'array'],
        items: {
            type: 'string',
        },
    },
    email: {
        type: 'string',
        format: 'email',
    },
    nip: {
        type: 'string',
        pattern: '^[0-9]{10}$',
    },
    zip_code: {
        type: 'string',
        pattern: '[0-9]{2}-[0-9]{3}',
    },
    ip: {
        type: 'string',
        format: 'ip',
    },
    date: {
        type: 'string',
        format: 'date',
    },
    date_range: {
        type: 'array',
        minLength: 1,
        maxLength: 2,
        items: {
            format: 'date',
        },
    },
    dateOrNull: {
        type: ['string', null],
        format: 'date',
    },
    time: {
        type: 'string',
        format: 'time',
    },
    datetime: {
        type: 'string',
        format: 'date-time',
    },
    amount: {
        type: 'number',
        minimum: 0.01,
    },
    quantity: {
        type: 'integer',
        minimum: 1,
        maximum: 100,
    },
    integer: {
        type: 'integer',
    },
    integerOrNull: {
        type: ['integer', 'null'],
    },
    integerPositive: {
        type: 'integer',
        minimum: 1,
    },
    password: {
        type: 'string',
        minLength: 6,
        allOf: [
            {pattern: '[A-Z]'},
            {pattern: '[a-z]'},
            {pattern: '[0-9]'},
            {pattern: '[\?\!@#\$%\^&*\(\)<>\+\{\}\|_-]'}, // eslint-disable-line
        ],
    },
    integerOrArrayOfIntegers: {
        type: ['integer', 'array'],
        items: {
            type: 'integer',
        },
    },
    numberOrArrayOfNumbers: {
        type: ['number', 'array'],
        items: {
            type: 'number',
        },
    },
    ArrayOfNumbers: {
        type: ['array'],
        items: {
            type: 'number',
        },
    },
    ArrayOfInt: {
        type: ['array'],
        items: {
            type: 'integer',
        },
    },
    ArrayOfStrings: {
        type: ['array'],
        items: {
            type: 'string',
        },
    },
    country: {
        type: 'string',
        pattern: '[A-Z]',
        minLength: 2,
        maxLength: 2,
    },
    file: {
        type: 'object',
        properties: {
            filename: {
                type: 'string',
            },
            encoding: {
                type: 'string',
            },
            filePath: {
                type: 'string',
            },
            mimetype: {
                type: 'string',
            },
            tmpName: {
                type: 'string',
            },
        },
    },
    url: {
        type: 'string',
        format: 'uri',
    },
    phone: {
        type: 'string',
        pattern: '^\\d+$',
        minLength: 1,
        maxLength: 12,
    },
    phone9Digits: {
        type: 'string',
        pattern: '\\d+',
        minLength: 9,
        maxLength: 9,
    },
    phoneGlobal: {
        type: 'string',
        pattern: '\[0-9]{1,3}[0-9]{9,10}', //eslint-disable-line
    },
    imageBase64: {
        type: 'string',
        pattern: 'data:image\/(png|jpg|jpeg);base64,([^\s]+)', //eslint-disable-line
    },
    imageBase64OrNull: {
        type: ['string', null],
        pattern: 'data:image\/(png|jpg|jpeg);base64,([^\s]+)', //eslint-disable-line
    },
    fileBase64(type) {
        return {
            type: 'string',
            pattern: `data:application\/${type};base64,([^\s]+)`, //eslint-disable-line
        };
    },
}

module.exports = { ValidatorHelper };
